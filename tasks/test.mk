.PHONY: test test.clean

test.clean:
	$(call LOG_INFO,Removing $(TESTDIR)/*.exe and $(TESTDIR)/*.o')
	$(RMDIR) $(TESTDIR)/*.exe $(TESTDIR)/*.o

clean: test.clean

## test: Run tests
test: deps-dev test.clean tests/all_test.exe
	$(call LOG_INFO,Running '$@')
	tests/all_test.exe

tests/all_test.exe: vendor/libtap/tap.o lib/libmain.a
