## deps-dev: Install dev dependencies
deps-dev: deps

deps-dev: $(VENDORDIR)/libtap

$(VENDORDIR)/libtap:
	$(call LOG_INFO,Downloading '$@')
	$(GITCLONE) https://github.com/zorgnax/libtap $@ > /dev/null
