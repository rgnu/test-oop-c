## deps: Install dependencies
deps:

deps.clean:
	$(call LOG_INFO,Removing '$(VENDORDIR)')
	$(RMDIR) $(VENDORDIR)

clean: deps.clean