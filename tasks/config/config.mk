.SILENT:

# Global configuration
SHELL=bash -o pipefail
TASKDIR?=$(CURDIR)/tasks
BINDIR?=$(CURDIR)/bin
DISTDIR?= $(CURDIR)/dist
VENDORDIR?=$(CURDIR)/vendor
TESTDIR?=$(CURDIR)/tests
VERSION?=$(shell git describe --dirty)

# Commands used in tasks
ECHO?=echo -e
NOP?=$(ECHO) >> /dev/null
CHMODX?=chmod +x
SHASUM256?=shasum -a 256
CAT?=cat
AWK?=awk
COPY?=cp -a
MKDIR?=mkdir -p
RMDIR?=rm -rf
GITCLONE?=git clone
SED?=sed
SORT?=sort

CC := gcc
CFLAGS := -g -Wall
INCLUDES := -I$(CURDIR)/include -I$(VENDORDIR)
CMD := $(addsuffix .exe,$(basename $(wildcard cmd/*)))
