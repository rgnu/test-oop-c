## build: Build binaries
build: $(CMD)

## clean: Clean binaries
clean:
	$(call LOG_INFO,Removing '$(CMD)')
	rm -f $(CMD)

# this is a suffix replacement rule for building .o's from .c's
# it uses automatic variables $<: the name of the prerequisite of
# the rule(a .c file) and $@: the name of the target of the rule (a .o file) 
# (see the gnu make manual section about automatic variables)
%.o: %.c
	$(call LOG_INFO,Compiling '$@')
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

%.exe: %.o
	$(call LOG_INFO,Building '$@')
	$(CC) $(CFLAGS) -o $@ $+

%.d: %.c
	@set -e; rm -f $@; \
		$(CC) -MM $(CFLAGS) $(INCLUDES) $< > $@.$$$$; \
		sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
		rm -f $@.$$$$

lib/libmain.a: $(addsuffix .o,$(basename $(wildcard lib/*.c)))
	$(call LOG_INFO,Creating lib '$@')
	ar rcs $@ $+

cmd/test-list.exe: lib/libmain.a
cmd/test-address.exe: lib/address.o
cmd/test-person.exe: lib/person.o lib/address.o
cmd/test-citizen.exe: lib/citizen.o lib/person.o lib/address.o
