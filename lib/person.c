#include <stdio.h>
#include <stdlib.h>
#include <person.h>
#include <address.h>

struct person {
	char* name;
	address_t address;
};

person_t person_new(char* name,  address_t address) {
    person_t self;

    self = (person_t) malloc( sizeof(*self) );

    self->name    = name;
    self->address = address;

    return self;
}

void person_destroy(person_t self) {
	address_destroy(self->address);
    free(self);
}

char* person_get_name(person_t self) {
	return self->name;
}

address_t person_get_address(person_t self) {
	return self->address;
}

void person_talk(person_t self) {
	printf("Hi, my name is %s\n", person_get_name(self));
}

void person_location(person_t self) {
	address_t address = person_get_address(self);

	printf(
		"I’m at %s %s %s %s %s\n",
		address_get_number(address),
		address_get_street(address),
		address_get_city(address),
		address_get_state(address),
		address_get_zip(address)
	);
}

void person_to_string(person_t self) {
    printf(
        "person[name=%s, address=]",
        person_get_name(self)
    );
}
