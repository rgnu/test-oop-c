#include <stdio.h>
#include <stdlib.h>
#include <list.h>

struct list {
  int    size;
  node_t head;
};

list_t list_new() {
    list_t obj;

    obj = (list_t) malloc( sizeof(*obj) );

    obj->head = NULL;
    obj->size = 0;

    return obj;
}

void list_destroy(list_t obj) {

    node_t node = obj->head;
    node_t next;

    while(node) {
        next = node_get_next(node);
        node_destroy(node);
        node = next;
    }

    free(obj);
}

void list_set_head(list_t obj, node_t head) {
    obj->head = head;
}

node_t list_get_head(list_t obj) {
    return obj->head;
}

void list_push(list_t obj, int value)
{
    node_t node;
    
    node  = node_new(value);
    node_set_next(node, list_get_head(obj));
    list_set_head(obj, node);
    list_set_size(obj, list_get_size(obj) + 1);
}

int list_shift(list_t obj)
{
    node_t node;
    int    value;

    node  = list_get_head(obj);
    value = (int) NULL;
    
    if (node) {
        value = node_get_value(node);

        list_set_head(obj, node_get_next(node));

        list_set_size(obj, list_get_size(obj) - 1);

        node_destroy(node);
    }

    return value;
}

int list_get_size(list_t obj)
{
    return obj->size;
}

void list_set_size(list_t obj, int size)
{
    obj->size = size;
}

void list_dump(list_t obj)
{
    node_t node;
    
    node = list_get_head(obj);
    
    printf("[");
    
    while(node) {
        printf("%d ", node_get_value(node));
        node = node_get_next(node);
    }
    
    printf("]\n");
}