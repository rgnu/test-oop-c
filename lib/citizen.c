#include <stdio.h>
#include <stdlib.h>
#include <person.h>
#include <address.h>
#include <citizen.h>

struct citizen {
	person_t person;
	char* country;
};

citizen_t citizen_new(char* name,  char* country, address_t address) {
    citizen_t self;

    self = (citizen_t) malloc( sizeof(*self) );

	self->person  = person_new(name, address);
    self->country = country;

    return self;
}

void citizen_destroy(citizen_t self) {
	person_destroy(self->person);
    free(self);
}

char* citizen_get_name(citizen_t self) {
	return person_get_name(self->person);
}

address_t citizen_get_address(citizen_t self) {
	return person_get_address(self->person);
}

char* citizen_get_country(citizen_t self) {
	return self->country;
}

void citizen_nationality(citizen_t self) {
	printf(
		"%s is a citizen of %s\n",
		citizen_get_name(self),
		citizen_get_country(self)
	);
}

void citizen_talk(citizen_t self) {
	printf("Hello, my name is %s and I'm from %s\n", citizen_get_name(self), citizen_get_country(self));
}

void citizen_location(citizen_t self) {
	person_location(self->person);
}

void citizen_to_string(citizen_t self) {
    printf(
        "citizen[name=%s, country=%s]",
        citizen_get_name(self),
		citizen_get_country(self)
    );
}
