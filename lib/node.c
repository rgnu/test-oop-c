#include <stdlib.h>
#include <node.h>

struct node {
  int    value;
  struct node * next;
};

node_t node_new(int value) {
    node_t obj;

    obj = malloc( sizeof(*obj) );
    
    obj->value = value;
    obj->next  = NULL;

    return obj;
}

void node_destroy(node_t obj) {
    free(obj);
}

void node_set_value(node_t obj, int value) {
    obj->value = value;
}

int node_get_value(node_t obj) {
    return obj->value;
}

void node_set_next(node_t obj, node_t next) {
    obj->next = next;
}

node_t node_get_next(node_t obj) {
    return obj->next;
}
