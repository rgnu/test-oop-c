#include <stdio.h>
#include <stdlib.h>
#include <address.h>

struct address {
	char* number;
	char* street;
	char* city;
	char* state;
	char* zip;
};

address_t address_new(char* number,  char* street,  char* city, char* state, char* zip) {
    address_t obj;

    obj = (address_t) malloc( sizeof(*obj) );

    obj->number = number;
    obj->street = street;
    obj->city   = city;
    obj->state  = state;
    obj->zip    = zip;

    return obj;
}

void address_destroy(address_t obj) {
    free(obj);
}

char* address_get_number(address_t obj) {
	return obj->number;
}

char* address_get_street(address_t obj) {
	return obj->street;
}

char* address_get_city(address_t obj) {
	return obj->city;
}

char* address_get_state(address_t obj) {
	return obj->state;
}

char* address_get_zip(address_t obj) {
	return obj->zip;
}

void address_to_string(address_t obj) {
    printf(
        "address[number=%s, street=%s, city=%s, state=%s, zip=%s]",
        address_get_number(obj),
        address_get_street(obj),
        address_get_city(obj),
        address_get_state(obj),
        address_get_zip(obj)
    );
}
