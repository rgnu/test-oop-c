#include <libtap/tap.h>
#include <citizen.h>

int citizen_test () {
    address_t a = address_new("1099", "Rivadavia", "CABA", "Buenos Aires", "1033");
    citizen_t r = citizen_new("Esteban Quito", "Argentina", a);

    ok(r != NULL,                              "lib > citizen > create a new citizen");
    ok(citizen_get_name(r) == "Esteban Quito", "lib > citizen > can get name");
    ok(citizen_get_country(r) == "Argentina",  "lib > citizen > can get country");

    citizen_destroy(r);

    return 0;
}