#include <libtap/tap.h>
#include <list.h>

int list_test () {
    list_t r = list_new();

    ok(r != NULL, "lib > list > create a new list");

    ok(list_get_size(r) == 0, "lib > list > initial size should be 0");

    list_push(r, 23);
    ok(list_get_size(r) == 1, "lib > list > size after push should be 1");

    ok(list_shift(r) == 23, "lib > list > last value should be 23");
    ok(list_get_size(r) == 0, "lib > list > size after shift should be 0");

    list_destroy(r);

    return 0;
}