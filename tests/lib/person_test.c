#include <libtap/tap.h>
#include <person.h>

int person_test () {
    address_t a = address_new("1099", "Rivadavia", "CABA", "Buenos Aires", "1033");
    person_t r  = person_new("Esteban Quito", a);

    ok(r != NULL,                             "lib > person > create a new person");
    ok(person_get_name(r) == "Esteban Quito", "lib > person > can get name");
    ok(person_get_address(r) == a,            "lib > person > can get address");


    person_destroy(r);

    return 0;
}