#include <libtap/tap.h>
#include <address.h>

int address_test () {
    address_t r;

    ok((r = address_new("2069", "Rivadavia", "CABA", "Buenos Aires", "1033")) != NULL, "lib > address > create a new address");
    ok(address_get_zip(r) == "1033", "lib > address > can get zip");
    ok(address_get_street(r) == "Rivadavia", "lib > address > can get street");

    address_destroy(r);

    return 0;
}