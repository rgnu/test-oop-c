#include "lib/address_test.c"
#include "lib/person_test.c"
#include "lib/citizen_test.c"
#include "lib/list_test.c"

int lib_test () {
    address_test();
    person_test();
    citizen_test();
    list_test();
    return 0;
}

#ifndef __MAIN__
#define __MAIN__
int main () {
    lib_test();
    done_testing();
}
#endif