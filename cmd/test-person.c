#include <stdio.h>
#include <stdlib.h>
#include <address.h>
#include <person.h>

int main()
{
    person_t p;

    p = person_new("Esteban Quito", address_new("1099", "Rivadavia", "CABA", "Buenos Aires", "1033"));
    person_location(p);
    person_talk(p);
}
