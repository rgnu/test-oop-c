#include <stdio.h>
#include <stdlib.h>
#include <list.h>

int main()
{
    /* This will be the unchanging first node */
    list_t list;

    /* Now root points to a node struct */
    list = list_new();
    list_push(list, 5);
    list_push(list, 10);
    list_push(list, 20);
    list_push(list, 40);
    
    list_dump(list);
    
    printf("Last %d\n", list_shift(list));
    
    list_dump(list);

    list_destroy(list);
}
