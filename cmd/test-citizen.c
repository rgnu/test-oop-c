#include <stdio.h>
#include <stdlib.h>
#include <address.h>
#include <citizen.h>

int main()
{
    citizen_t c;

    c = citizen_new(
        "Esteban Quito",
        "Argentina",
        address_new("1099", "Rivadavia", "CABA", "Buenos Aires", "1033")
    );

    citizen_talk(c);
    citizen_nationality(c);
    citizen_location(c);
}
