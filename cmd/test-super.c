#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef struct foo_s {
    int a;
} foo;

typedef struct bar_s {
    foo super;
    int b;
} bar;

typedef foo* foo_t;

typedef bar* bar_t;

int foo_get_a(foo_t self) {
  return self->a;
}

void foo_set_a(foo_t self, int a) {
  self->a = a;
}

void foo_init(foo_t self, int a) {
    foo_set_a(self, a);
}


foo_t foo_new(int a) {
   foo_t self = (foo_t) malloc( sizeof(*self) );

    foo_init(self, a);

    return self;
}

void bar_set_b(bar_t self, int b) {
  self->b= b;
}


void bar_init(bar_t self, int a, int b) {
    foo_init(&self->super, a);
    bar_set_b(self, b);
}

bar_t bar_new(int a, int b) {
    bar_t self = (bar_t) malloc( sizeof(*self) );
    bar_init(self, a, b);
    return self;
}



// Main
int main() {
    bar_t b = bar_new(5, 1);
    foo_t f = foo_new(10);
    assert(foo_get_a((foo_t) b) == 5);
    assert(foo_get_a((foo_t) f) == 10);

    printf(
        "result: b.a=%d f.a=%d\n",
        foo_get_a((foo_t) b),
        foo_get_a((foo_t) f)
    );
  return 0;
}