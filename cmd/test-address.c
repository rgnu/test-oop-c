#include <stdio.h>
#include <stdlib.h>
#include <address.h>

int main()
{
    address_t address;

    address = address_new("2069", "Rivadavia", "CABA", "Buenos Aires", "1033");
    address_to_string(address);
    address_destroy(address);
}
