all: help

# Include general config
include $(wildcard $(CURDIR)/tasks/config/*.mk)
include $(wildcard $(CURDIR)/tasks/*.mk)

help: ## Shows this help
	$(ECHO) 'Available targets are:' \
	&& sed -n -e "s|^\([[:alpha:]].*\): ## \(.*\)|\1##\2|p" -e "s|^## \([[:alpha:]].*\): \(.*\)|\1##\2|p"$(MAKEFILE_LIST) \
	| $(AWK) 'BEGIN {FS = "##"}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' \
	| sort
