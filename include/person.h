#ifndef PERSON_H
#define PERSON_H

#include <address.h>

typedef struct person * person_t;

person_t person_new(char* name,  address_t address);

void person_destroy(person_t self);

char* person_get_name(person_t self);

address_t person_get_address(person_t self);

void person_talk(person_t self);

void person_location(person_t self);
#endif
