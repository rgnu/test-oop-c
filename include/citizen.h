#ifndef CITIZEN_H
#define CITIZEN_H

#include <address.h>

typedef struct citizen * citizen_t;

citizen_t citizen_new(char* name,  char* country, address_t address);

void citizen_destroy(citizen_t self);

char* citizen_get_name(citizen_t self);

address_t citizen_get_address(citizen_t self);

char* citizen_get_country(citizen_t self);


void citizen_talk(citizen_t self);

void citizen_location(citizen_t self);

void citizen_nationality(citizen_t self);

void citizen_to_string(citizen_t self);

#endif
