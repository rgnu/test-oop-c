#ifndef NODE_H
#define NODE_H

typedef struct node * node_t;

node_t node_new(int value);
void node_destroy(node_t obj);
void node_set_value(node_t obj, int value);
int node_get_value(node_t obj);
void node_set_next(node_t obj, node_t next);
node_t node_get_next(node_t obj);

#endif
