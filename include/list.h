#ifndef LIST_H
#define LIST_H

#include <node.h>

typedef struct list * list_t;

list_t list_new();

void list_destroy(list_t obj);

int list_get_size(list_t obj);

void list_push(list_t obj, int value);

int list_shift(list_t obj);

void list_dump(list_t obj);

#endif