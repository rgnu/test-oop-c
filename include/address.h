#ifndef ADDRESS_H
#define ADDRESS_H

typedef struct address * address_t;

address_t address_new(char* number,  char* street,  char* city, char* state, char* zip);

void address_destroy(address_t obj);

char* address_get_number(address_t obj);

char* address_get_street(address_t obj);

char* address_get_city(address_t obj);

char* address_get_state(address_t obj);

char* address_get_zip(address_t obj);

void address_to_string(address_t obj);

#endif